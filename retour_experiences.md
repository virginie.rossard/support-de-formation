# Retours d'expérience suite formations

| Date  | Qui  | Combien | Mode | Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  5 Juin 2023 | Olivier AgroClim | 10 | RStudio| 1h15 | 1

- il faut que les ordinateurs aient Git installé au début, sinon on perd du temps
- il y a redondance entre À quoi sert Git ? / Bénéfices / Cas d'utilisation
- « Les états du système de fichiers » pourrait être enlevé, il y a « états (session 1) » ensuite.
- 1h, c'est un peu court, nous avons fini en 1h15 et j'ai pressé sur... les étiquettes
- les étiquettes, c'est un usage avancé, utile que pour certains cas
- utiliser la touche S pour avoir l'affichage Note du présentateur.

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  5 Juin 2023 | Philippe URFM | 7  | 5 Rstudio, 2 Vscode |1h05 | 1|

- Nécessité d'avoir les outils déployés ou présence de l'IP 
- Il y a redondance entre "À quoi sert Git ? / Bénéfices / Cas d'utilisation": pour moi, ça permet d'insister sur ces concepts forts
- « Les états du système de fichiers » pourrait être placé à la fin de la session 2
- 1h, c'est Ok avec le contenu. 
- les étiquettes: Je suis également passé rapidement là-dessus. Mais j'ai insisté sur l'avantage qu'on pouvait en tirer. Ils ont trouvé par eux-mêmes où apparaissaient les étiquettes dans leur interface Rstudio !

Au bilan, déçu de ne pas avoir eu plus de monde, mais c'était pour se faire la main, donc ce sera encore mieux la fois prochaine.
J'ai reproposé une session la semaine suivante, une seule personne est venue. Elle connaissait déjà git !

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  19 Juin 2023 | Philippe URFM | 4  | gitlab |1h30 | 2|

- Une personne présente qui n'avait pas suivi la session 1: on a rapidement créé un projet sur son poste. Pas trop perturbant 
- quelques points à ajouter dans certaines diapos (anecdotique)
- La majorité des git init lancé sur les postes à créé une branche master. Rajouter la commande de renommage (master --> main) et être vigilant lors du git init pour éviter ce piège, qui amène les utilisateurs devant un message difficile à comprendre quand on n'a pas d'expérience.
- On a eu tout juste le temps. Ne pas prévoir moins d'1h30 pour cette session 
- les commandes initiales pour faire le premier push ne sont pas assez explicite pour certains. En particulier le cd existing_project. Une personne l'a tapée telle quelle !
- la création de la clef prend du temps ...

Au bilan, encore une fois, déçu de ne pas avoir eu plus de monde.

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  23 Octobre 2023 | Philippe URFM | 2  | Branches |1h30 | 2|

- Très peu de monde ...
- Vu presque toute la présentation, sans pratique
- j'étais venu avec un TP "plus simple" qu'il faudra partager et critiquer pour optimiser le temps de pratique
- Dans vscode, je constate que des branches distantes (toutes ?)apparaissent alors que gitlab ne les montre pas. Je suppose que vscode en conserve la trac quelque part ...
- D62: rajouter une diapo mermaid avec le graphe correspondant (un plus il me semble)
- D64: quand on lit le message de conflit, on ne voit apparaitre qu'un seul nom de branche (patch-1). Est-ce que c'est parce que c'est avec main ? Que signifie HEAD dans ce contexte. Une des personnes présentes à fait un test après le TP, et le message de conflit ne donne le nom d'aucune des branches mergées. Option ?
- Quand on merge depuis l'IHM de gitlab et qu'un conflit apparait, comment doit-on gérer pour le résoudre puisque le dépôt local ne sait pas qu'on a cherché à faire un merge et donc l'éditeur ne peut pas le montrer ? Un fetch peut-être ?

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
| 2 octobre 2023 | Olivier AgroClim | 8 | GitLab + RStudio + VS Code + ligne de commande | 2h | 3 |

- Créer la bifurcation du projet avant et le configurer (méthode de fusion, Squasher les commits lors de la fusion, manque un cadrage précis sur ce point).
  - Nous nous sommes retrouvés à ne pas avoir le bouton _Créer une demande de fusion_ depuis les tickets. Cela a été l'occasion de montrer une autre voie, mais a brouillé un peu le discours sur le coup.
- Fournir le corrigé.
- Basculer sur le TP rapidement au cours du déroulé des diapos.
- Toutes les diapos ont été passées en 2h ainsi que le travail sur les branches et fusions.
- Demandes pour la suite :
  - un TP uniquement sur Rebase et fusion
  - les pipelines : ce que c'est, quelles sont les possibilités, quelques exemples

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  06 décembre 2023 | Philippe URFM | 3  | Tous | 1h45 | 1|

- 3 scientifiques présents.
- il a fallu installer le client Git sur 2 postes mais peu chronophage au final
- 1 scientifique utilise SVN et trouve Git plus complexe à aborder. La question du repository présent à la fois en local et en distant contribue à ce trouble.
- Correction d'une typo dans les lignes de commandes (ligne 80 de 04_ligne de commande_complet.md)
- une durée allongée, permet une discussion à la fin, pour se projeter sur la suite.



| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  14 décembre 2023 | Philippe URFM | 4  | Web + LC| 2h15 | 2|

- 4 scientifiques présents.
- Quelques questions posées (cf [issue 28](https://forgemia.inra.fr/git-gitlab-paca/support-de-formation/-/issues/28) )
- Problème à la création des repos locaux (branche master par défaut), du coup, proposition pour reprendre et clarifier le code (cf [issue 29](https://forgemia.inra.fr/git-gitlab-paca/support-de-formation/-/issues/29))
- Problème pour l'affichage de la clef publique (typo corrigée directement dans le main)
- trop bavard, on n'a pas eu le temps de bien tester les issues ...


| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
|  21 décembre 2023 | Philippe URFM | 3  | Web + LC| 2h15 | 3|

- 3 scientifiques présents
- parcours rapide des diapos et tests avec le repo exercise_session3 forké dans collection URFM
- diapos non parcourues: gestion des branches en local
- pas eu le temps d'aller jusqu'au test du rebasage

| Date  | Qui  | Combien | Mode |  Durée |Session |
|:-:    |:-:    |:-:    |:-:    |:-:    |:-:    |
| 25 avril 2024 | Olivier AgroClim & Philippe URFM / UEFM | 12 | RStudio | 1h45 | 1 |

- stagiaires, CDD, doctorants, post-doc, scientifiques : 3 AgroClim, 8 URFM, 1 UEFM
- RStudio et Git installés avant la formation
  - RStudio fraîchement installé a dysfonctionné sur 2 portables : la création de projet bloque en choisissant Git ou RStudio n'affiche pas l'onglet Git
- La mention de *HEAD* (`02_01_etats_session1_git.md`) m'a semblé inutile à ce moment de la présentation
- L'option *Sign* dans la fenêtre de Commit de RStudio (une nouvelle option par rapport à la capture d'écran) a soulevé des questions auprès d'une étudiante.
