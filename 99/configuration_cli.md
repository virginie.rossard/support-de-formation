---slide---

# Configuration générale de Git en ligne de commande

---vertical---

## Configurer le nom d'utilisateur

A faire au avant de commencer à travailler.

```bash
git config --global user.name "Prénom Nom"
git config --global user.email "Prenom.Nom@inrae.fr"
```

> L'option `--global` permet de répercuter l'option de configuration sur tous les projets git de l'utilisateur.

Cette configuration est sauvée dans le fichier `.gitconfig`

---vertical---

## Configuration système

Cette configuration impacte tous les utilisateurs d'un système, on la trouve généralement dans `/etc/gitconfig` et on l'atteint en utilisant l'option `--system`et nécessite d'avoir les droits d'écriture sur le système.

---vertical---

## L'éditeur (pour les commit)
```
git config --global core.editor vi
```

---vertical---

## Modèle pour les commit
```
git config --global commit.template mon_template.txt
```

---vertical---

## Liste des fichiers à ignorer (pour tous les projets)
```
git config --global core.excludesfile ~/.gitignore_global
```
