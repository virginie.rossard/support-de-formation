---slide---

# Git : commandes principales

* `git init` : initialise un dossier en tant que « working copy » ->mkdir .git
* `git clone git@forgemia.inra.fr:urfm/plantaexp.git` : obtient une copie de travail du contenu disponible dans la forge pour le projet plantaexp [ s’il est accessible…]

---vertical---

* `git status` : affiche les modifications sont en attente de validation
* `git add ./toto.txt` : ajoute le fichier toto.txt à la liste des fichiers suivis
* `git commit -m "un texte descriptif"` : valide les modifications en attente dans la working copy en y associant un texte explicatif de ce qui a été fait dans le code
* `git diff` : montre les changements sur les fichiers
* `git push origin master` : remonte les modifications locales [ de la branche sélectionnée ] vers le repository central dans la branche master

Le **commit** est l'élément central de Git. 

Un **commit** représente un ensemble cohérent de modifications.

---vertical---


# Le commit

## Le message

Le message d'un commit suivra le modèle suivant :

- Le titre (49 caractère), explicite. On utilise des raccourcis

  - *fix:* pour correctif

  - Rien pour une nouvelle fonctionnalité (ou *feat:*)

  - Ne pas utiliser de mots vagues

  - On commence par le contexte suivi par les actions

    Exemple : Au lieu d'écrire *Fonctionnalité : Je teste si l'élève a bien l'exercice pour lui afficher le suivant*

    on écrira : *Élève/exercice : si terminé, afficher suivant*

    ​						**Contexte**						**Action**

- La description, aussi longue que nécessaire. Séparée d'une ligne vide du titre

Attention, le commit en ligne ne permet pas de séparer le titre de la description. 

```
git commit -m "Elève/exercice : si terminé, afficher suivant"
```

Il faudra utiliser un modèle de commit et spécifier un éditeur.

# Manipulation de l'historique

La commande `git log` permet d'afficher tout l'historique du projet. Pour limiter aux derniers commits, on peut  utiliser la commande:

```
git log -1
```

 Où le chiffre indique le nombre de commit à afficher.

```
git log --after="2022-09-30"	# L'historique après une date
git log --since=1.weeks
git log --author "Granier"		# Par auteur
git log -3 --author "Granier"	# Les 3 derniers commits de Granier
```

# Comparaison entre versions

```
git diff
```

compare la version du répertoire de travail et de l'index. 

```
git diff --cached # Compare l'index et le dernier commit (vide si on vient juste de commiter)
git diff HEAD # Compare le répertoire de travail et le dernier commit
```

> HEAD correspond au dernier commit de la branche courante

Différences entre différents commit

1. identifier le hash des commit à comparer

```
git log --oneline -3
52ccb30 (HEAD -> main) Manipulation de l'historique
79db7e0 Message d'un commit
c42aba1 Correction orthographe
```

Si l'on souhaite comparer la version "Message d'un commit" et "Correction orthographe"

```
git diff c42aba1 79db7e0 # pour une meilleure compréhension, mettre le plus vieux commit en premier
git diff c42aba1 HEAD # différence entre le commit "Correction orthographe" et le dernier commit de la branche courante
```

# Identifier l'auteur d'une ligne de code

La commande `git blame fichier.txt` permet d'afficher le contenu du fichier ligne par ligne avec le hash du commit où été ajoutée la ligne, l'auteur et la date  

# Suppression de modifications

Revenir à a dernière version du dépôt

```
git reset --hard HEAD
```

Dernière version de l'index

```
git checkout -- fichier
git restore fichier 		# équivalent (commande introduite dans git 2.24)
```

Remplacer l'index par la dernière version du dépôt

```
git reset HEAD	# sans --hard, on conserve les modifications du répertoire de travail
```

# Revenir à un état antérieur

1. L'identifier

   ```
   git log --before="2022-11-08"
   ```

2. Récupérer le hash et lancer

   ```
   git reset hash
   ```

   Cette commande ramène le dépôt au niveau du contenu du commit correspondant au hash. Si on omet `--hard`, le répertoire de travail n'est pas modifié et peut donc être différent du dépôt.

# Modifier le dernier commit

```
git commit --amend
```

