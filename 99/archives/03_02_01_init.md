---slide---

# Initialisation d’un dépôt git (Rstudio)

---vertical---

Lors de la création d'un projet RStudio

![Initialiser un dépôt lors de la création d'un projet RStudio](images/03_02_01_init_avec_un_projet_rstudio.png)

---vertical---

Dans un projet existant (1/3)

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant.png)

---vertical---

Dans un projet existant (2/3)

Choisir "git" dans le menu "version control system"

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant2.png)

---vertical---

Dans un projet existant (3/3)

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant3.png)

---vertical---

Git est désormais intégré au projet :

![Vue git dans RStudio](images/03_02_01_init_resultat.png)

---vertical---

L'onglet git de RStudio vous permet de connaître l'état du dépôt rattaché à votre projet :

![Onglet git dans RStudio](images/03_02_01_onglet_git.png)

---vertical---

On voit dans l'interface que le fichier `.gitignore` n'est pas encore suivi par git : icône orange avec un "`?`" et état "untracked"

![État du gitignore après initialisation dans RStudio](images/03_02_01_etat_gitignore.png)

---vertical---

TODO : parler ici de la commande `$ git status`

---vertical---

Le fichier `.gitignore` permet de préciser quels fichiers doivent être ignorés par git.
Par défaut, il contient les fichiers suivants :
```
.Rproj.user
.Rhistory
.RData
.Ruserdata
```

Voyons comment l'ajouter au dépôt avec un commit !
