---slide---

# Annuler des modifications non commitées (RStudio)

---vertical---

Modifier un fichier sans faire de commit sur les modifications :

![Fichier avec quelques lignes en plus](images/03_02_05_modification_fichier.png)

---vertical---

On peut aller dans l'interface de commit pour voir les modifications :

![Vue des modifications dans l'interface de commit](images/03_02_05_vue_modifications.png)

---vertical---

Si l'on souhaite supprimer **toutes** les modifications non commitées, on peut faire un clic-droit sur le fichier concerner et faire un "revert" :

![Clic droit sur un fichier en vue d'un revert](images/03_02_05_revert.png)

---vertical---

Attention, c'est tout ce qui a été modifié dans le fichier depuis le dernier commit qui sera supprimé :

![Clic droit sur un fichier en vue d'un revert](images/03_02_05_revert_warning.png)

---vertical---

Pour annuler les modifications non commitées sur un fichier, on peut utiliser la commande suivante :

```bash
git restore <nom du fichier>
```
