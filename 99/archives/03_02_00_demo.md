---slide---

# Démonstration des opérations de base avec RStudio

Prérequis : travailler dans un projet [RStudio](https://posit.co/download/rstudio-desktop/)

[Documentation RStudio](https://support.posit.co/hc/en-us/articles/200532077)

---vertical---

Créer un projet

![Créer un projet](images/03_02_00_demo_creation_projet.png)

---vertical---

Ouvrir un projet existant

![Ouvrir un projet existant](images/03_02_00_demo_ouverture_projet.png)
