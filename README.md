# Support de formation

Le code source comprend le contenu d'une présentation sur Git.

Le plan de la formation est disponible dans le fichier [`plan.md`](plan.md).
Chaque sessions dispose d'une présentation écrite dans des dossiers séparés.

# Description technique

Le utilise la bibliothèque JavaScript [RevealJS](https://revealjs.com/) pour la création des présentations.
Les fichiers de cette bibliothèque sont présents dans le dossier `public/revealjs/`.

Le diaporama est publié sur GitLab Pages, fonctionnalité intégrée à la forge, à <https://git-gitlab-paca.pages.mia.inra.fr/support-de-formation/>.

# Modification en local

Pour lancer le diaporama sur son poste :

- `git clone git@forgemia.inra.fr:git-gitlab-paca/support-de-formation.git`
- `cd support-de-formation`
- Sous Linux : `make clean; make run`

Pour seulement construire la présentation, utiliser `make build`.
`make clean` permet d'enlever la construction de la présentation. Nécessaire après des modifications

`./liveupdate.sh <dossier>` permet de
- construire la présentation
- en continu mettre à jour la présentation d'une session, définie par `<dossier>`
- afficher la présentation dans le navigateur.
Il suffit de rafraîchir le navigateur (touche F5) pour voir les modifications.

Exemple : `./liveupdate.sh 03_branches/`

# Licence

Licence ouverte [Etalab 2.0](https://forgemia.inra.fr/git-gitlab-paca/support-de-formation/-/blob/main/LICENCE.pdf?ref_type=heads)