---slide---


# Pré-requis pour cette session 2

Avoir suivi la session 1 ou connaître les commandes de base git

---vertical---
Nous allons partir d'un dépôt local que vous souhaitez gérer avec GitLab.

Si vous n'avez pas de projet, créez-en un :
* créer un dossier
* initialiser le dépôt git

Relire <a href="https://git-gitlab-paca.pages.mia.inra.fr/support-de-formation/01_initiation/" target="_blank">ici</a> comment on avait fait !
