---slide---

# Création de la clé SSH

---vertical---

## Définitions
* Secure Shell (SSH)
* Protocole réseau qui permet :
	* de connecter un utilisateur à un système distant
	* authentifier un utilisateur
	* chiffrer les communications
* Est composé d'une clé privée et d'une clé publique

---vertical---

## Création d'une clé SSH

* Sur Windows ouvrir un terminal de commande
* Pour créer une paire de clés SSH :
	* Ouvrir un terminal (Git Bash par exemple sous Windows)
	* `ssh-keygen -t ed25519`
	* (Ne pas rentrer de mot de passe)
	* Copier la clé : 
	* `ssh-agent sh -c 'ssh-add 2>/dev/null; ssh-add -L'`

Note: utiliser Git-GUI, menu *Help*

---vertical---

Coller la clé dans `Edit Profil` > `SSH Keys`, ne pas saisir de date d'expiration. Puis cliquer sur Add key

![Créer une clé SSH](images/05_04_creation_cle_ssh.png)


---vertical---

Plus d'informations sur <a href="https://forgemia.inra.fr/help/user/ssh.md" target="_blank">la page SSH de GitLab</a>.
