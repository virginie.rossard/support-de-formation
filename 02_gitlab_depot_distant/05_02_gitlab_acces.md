---slide---

# Configuration de l'accès à GitLab

Pour se connecter à GitLab :

* <a href="https://forgemia.inra.fr" target="_blank">ForgeMIA</a>
* Se connecter avec ses identifiants INRAE ("LDAP") en cliquant sur le bouton "connexion SSO"


Note: GitLab est disponible en français : [*Profile* > *Preferences* > *Localization*](https://forgemia.inra.fr/-/profile/preferences#localization)
