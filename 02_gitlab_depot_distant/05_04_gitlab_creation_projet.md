---slide---

# Création d'un projet dans GitLab

---vertical---
# Créer un nouveau projet

Sur la page d'accueil, cliquer sur le bouton "New project"

![Nouveau projet](images/05_04_gitlab_nouveau_projet.png)

---vertical---
Pour mettre sur GitLab un dépôt créé sur votre machine, cliquer sur "Create blank project"

![Projet vide](images/05_04_gitlab_blank_project.png)

---vertical---
# Paramètres du projet

On détermine le nom du projet (1) et son URL (2 et 3)

Note: (2) permet de choisir le groupe gitlab dans lequel on souhaite mettre son projet.
Pour un projet personnel, on peut utiliser simplement son nom d'utilisateur.

![Nom et url du projet](images/05_04_gitlab_creation_projet_1.png)

---vertical---
# Paramètres du projet

On détermine la visibilité du projet (1 : accès restreint, accès à tous les utilisateurs authentifiés, ou public)

Pour avoir un projet vide, ne pas créer de `README` (2)

![Visibilité du projet](images/05_04_gitlab_creation_projet_2.png)

---vertical---
# Projet créé !
![Vue d’un projet créé](images/05_04_gitlab_creation_projet_3.png)
