---slide---

# Projet GitLab

---vertical---

## Définition

Un *projet* sur GitLab contient :
- un *dépôt* git
- une interface pour voir l’historique des commits
- une interface pour visualiser les fichiers du dépôt
- un système de tickets (dit "issues")
- des membres
- beaucoup, *beaucoup* d’autres choses…

Note: le terme *issue* est traduit dans GitLab par « ticket ».
