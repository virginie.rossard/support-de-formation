---slide---

# Bonnes pratiques d'interaction

---vertical---

## Contrôler où on en est

* Après un moment d'absence, commencer par faire un ```git status```
* Si plusieurs personnes travaillent sur le même projet, commencer par un ```git pull``` (synchronisation du dépôt local).


---vertical---
## GitLab usine à gaz

* GitLab (son interface web) permet plein de chose mais faire simple !!
* Un bon projet n'exploite pas forcément toutes les fonctionnalités de GitLab

---vertical---
## Où éditer ses fichiers

* Même si GitLab le permet, ne pas modifier les fichiers depuis GitLab
* Si fait, commencer par un ```git pull``` avant les premières modifications
* Sinon, le prochain "push" sera refusé. Il faudra alors faire un pull et résoudre les éventuels conflits
