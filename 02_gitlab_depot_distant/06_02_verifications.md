---slide---

# Navigation sur un projet GitLab

Où s’y retrouver au milieu de toutes ces informations ?

---vertical---

![Navigation sur un projet GitLab](images/06_02_navigation_dans_projet.png)

---vertical---

## La vue d’ensemble

- Informations sur le dépôts (nb de commits, de branches, de tags, visibilité)
- Dernier commit
- Fichiers sur la branche `main`
- Affichage du `README.md`

---vertical---

![Vue d’ensemble](images/06_02_vue_ensemble.png)

---vertical---

## La vue du dépôt git

- Fichiers
- Commits
- Étiquettes

![Vue du dépôt](images/06_02_depot.png)

---vertical---

## Fichiers

![Vue des fichiers](images/06_02_fichiers.png)

---vertical---

## Commits

![Vue des commits](images/06_02_commits.png)

---vertical---

## Étiquettes

![Vue des tags](images/06_02_tags.png)

---vertical---

## Les issues

Une section dédiée est prévue un peu plus loin.

---vertical---

## La configuration du projet

La majorité des options par défaut est adaptée.

On peut ajouter un logo et une description au projet dans Settings > General.

---vertical---

![Configuration du projet](images/06_02_settings_general_1.png)

---vertical---

![Configuration du projet](images/06_02_settings_general_2.png)
