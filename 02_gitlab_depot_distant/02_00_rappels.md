---slide---

# Rappels de la session 1

Nous avons vu :

* intérêt de l'usage d'un outil comme git
* les différents états d'un dépôt local
* les commandes principales selon 3 modes
  * depuis l'interface de RStudio
  * en ligne de commande
  * avec l'éditeur VS Code

---vertical---
# Les commandes git de base (1/2)
* Initialiser un dossier en dépôt local (`git init`)
* Visualiser l'état d'un dépôt (`git status`)
* Comment ne pas suivre certains fichiers (`.gitignore`)
* Accepter des modifications (`git add`)
* Enregistrer des modifications (`git commit`)


---vertical---
# Les commandes git de base (2/2)

* Bonnes pratiques pour les commits (atomique, message)
* Consulter l'historique sur le dépôt (`git log`)
* Annuler des modifications (`git restore`, `git reset`)
* Voir des modifications (`git diff`)
