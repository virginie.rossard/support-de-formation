---slide---

# Gestion des étiquettes sur un dépôt distant

## avec RStudio

Utiliser le Shell (`Tools` > `Terminal` > `New Terminal`) avec la ligne de commande.

---vertical---

## en ligne de commande

### Transférer

Transférer une étiquette depuis un dépôt local :

```
git push origin [nom-du-tag]
```

Transférer toutes les étiquettes depuis un dépôt local :

```
git push origin --tags
```

### Supprimer

Supprimer une étiquette sur un dépôt distant :

```
git push origin --delete v1.4-lg
```

---vertical---


## avec VS Code

* Cliquer sur `Aide` > `Afficher toutes les commandes` ou utiliser le raccourci `F1` ou `Ctrl+Maj+P`.
* Saisir `Git: Push Tags`
* Appuyer sur `Entrée`

ou utiliser le *Terminal* avec la ligne de commande.
