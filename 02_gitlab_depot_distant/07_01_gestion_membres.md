---slide---

# Gestion des membres d'un projet dans GitLab

---vertical---

## Membres d’un projet

Pour inviter d’autres utilisateurs à contribuer au projet, il faut les y ajouter.

![Membres d’un projet](images/07_00_depot_distant_membres_1.png)

---vertical---

Pour ajouter un utilisateur de la forge au projet :

![Membres d’un projet](images/07_00_depot_distant_membres_2.png)

---vertical---

Pour configurer le status du membre :

![Membres d’un projet](images/07_00_depot_distant_membres_3.png)

Note: le compte ajouté doit pré-exister sur la forge (diapo suivante)

---vertical---

Lorsqu'un utilisateur n'est pas connu de la forge, il faut lui demander de faire une première connexion sur l'adresse <a href="https://forgemia.inra.fr" target="_blank">ForgeMIA</a>, avec son identifiant LDAP.

Il est alors connu de la forge, et vous pouvez l'ajouter sur le projet.

---vertical---

## Statut d’un membre

Recommandation pour les permissions :
* *guest* : invité, consultation uniquement
* *reporter* : suivi et contribution au projet sans coder
* *developer* : contribuer au code sans configurer le projet GitLab
* *maintainer* : contribuer au code et configurer le projet GitLab
* *owner* : pour le transfert de propriété du projet

---vertical---

## A vous de jouer

Invitez au moins une personne présente sur votre projet, en tant que reporter (ça suffira pour aujourd'hui)
