---slide---

# GitLab et ForgeMIA

* ForgeMIA : installation de GitLab sur les serveurs du département MathNum. En 2024, une forge institutionnelle INRAE verra le jour, basée sur la même solution technologique GitLab.

---vertical---

Forge :
> plateforme web simplifiant la mise en œuvre de bonnes pratiques et facilitant le développement de logiciels de meilleure qualité, ainsi que la constitution de communautés de contributeurs et d’utilisateurs <a href="https://hal-lara.archives-ouvertes.fr/hal-04098702" target="_blank">[hal-04098702]</a>.

---vertical---

Une forge intègre : <a href="https://hal-lara.archives-ouvertes.fr/hal-04098702" target="_blank">[hal-04098702]</a>

* outils de développement collaboratif (suivi des modifications du code, gestion des demandes et des réponses d’utilisateurs - tickets -, gestion des contributions, gestion du projet),
* industrialisation du processus de création du logiciel à partir de son code source (compilation, tests automatiques, assurance qualité, diffusion des livrables),
* outils de communication comme <a href="https://team.forgemia.inra.fr/" target="_blank">Mattermost</a>.
