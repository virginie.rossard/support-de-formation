---vertical---

## Remarque générale

Dans les lignes de commande, \<namespace\> est à remplacer par une valeur dépendante de votre contexte

Idem pour \<projet\>.git qui devra être remplacé par mon_projet.git si le projet s'appelle "mon_projet" !

---slide---

# Commandes initiales
## sur un nouveau dépôt

Les commandes sont affichées sur la page d'accueil du projet, lors de la création d'un dépôt vide.
3 cas :

* créer un dépôt local et l'envoyer
* envoyer un dépôt local existant
* cloner un dépôt distant en local

---vertical---

Cloner le dépôt distant en local avec
```sh
git clone git@forgemia.inra.fr:<namespace>/<projet>.git
```
Le dossier projet est créé en local et contient les fichiers du repository distant

---vertical---
Envoyer un dépôt local vers la forge 

_(version de Git > 2.28.0)_

En cas d'erreur sur la commande _git init_, passer à la diapo suivante
```sh
cd <projet>
git init --initial-branch=main
git remote add origin git@forgemia.inra.fr:<namespace>/<projet>.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

---vertical---
Envoyer un dépôt local vers la forge 

_(version de Git < 2.28.0)_

```sh
cd <projet>
git init
git checkout -b main
git remote add origin git@forgemia.inra.fr:<namespace>/<projet>.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

