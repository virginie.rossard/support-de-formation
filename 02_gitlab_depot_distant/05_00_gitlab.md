---slide---

# GitLab

GitLab : nom du logiciel libre développé par *GitLab Inc.* et utilisé sur la forge commerciale <a href="https://gitlab.com/" target="_blank">Gitlab.Com</a>.

Forge majoritaire dans les installations au sein de l'ESR <a href="https://hal-lara.archives-ouvertes.fr/hal-04098702" target="_blank">[hal-04098702]</a>.

Note: `hal-04098702` : *Forges de l’Enseignement supérieur et de la Recherche - Définition, usages, limitations rencontrées et analyse des besoins*. La définition de *forge* vient ensuite.
