---slide---

# *Issues*

Une *Issue* est comme un ticket et permet :
* prendre des notes
* d'échanger sur une idée
* d'identifier un problème et le documenter
* de structurer son plan de travail

Plus d'informations sur [les *issues*](https://docs.gitlab.com/ee/user/project/issues/)

Note: le terme *issue* est traduit dans GitLab par « ticket ».

---vertical---

Pour créer une *issue* : `Issues` > `New Issue`

![Ouvrir une issue](images/07_02_ouvrir_issue.png)

---vertical---

Les paramètres d'une *issue* :

* Titre
* Type (si template existant)
* Description
* Assignation
* Labels

---vertical---

![Paramètres d'une issue](images/07_02_creer_une_issue.png)

---vertical---

### Titre
Le titre permet d'avoir une vision rapide du problème ou de l'idée sur laquelle porte l'*issue*.

### Type
Si il existe, permet de sélectionner un template d'*issue*.

---vertical---

### Description
La description permet de décrire en profondeur l'idée ou le problème rencontré. C'est le corps de l'issue.
Elle s'écrit en Markdown :
* [Écrire en Markdown](https://docs.framasoft.org/fr/grav/markdown.html)
* [Markdown dans GitLab](https://forgemia.inra.fr/help/user/markdown)

Note: La syntaxe Markdown dans GitLab est étendue et propose des diagrammes et graphiques de flux, Emojis, équations et symboles mathématiques en LaTeX, listes de tâches, sommaires...

---vertical---

### Assignation
*Facultatif*. Permet d'assigner une/des personnes qui sera/seront à même de résoudre l'issue.

---vertical---

### Labels
Permet de :
* Catégoriser les *issues* à l'aide d'une couleur spécifique
* Filtrer rapidement les *issues*

---vertical---

### Créer des labels

![Créer des labels](images/07_02_creer_des_labels.png)

---vertical---
### A vous de jouer

2 scénarios d'*issues* sont proposés.

Testez les !

---vertical---

### Scénario 1

Sur votre projet, déclarez un bug :
* J'ouvre une *issue*
* Je la commente en y intégrant des captures d'écrans
* J'assigne la/les personnes les plus à même à résoudre le problème
* J'attribue le label `bug`

---vertical---

### Scénario 2

Sur le projet sur lequel vous avez été invité, proposez une nouvelle idée d'amélioration :
* J'ouvre une *issue*
* Je la commente en y intégrant :
	* des bouts de codes existants pouvant aider au développement
	* des cases à cocher pour suivre l'avancement
* Je m'assigne à l'*issue*
* Je crée et j'attribue le label `nouvelle fonctionnalité`

---vertical---

### Lien avec les commits

* Mentionner un commit dans un ticket (ex. : « Le texte ajouté par commit 26171abf doit être complété... »).
* Lier un commit à un ticket par le message de commit (ex. : `Correction orthographique. #123`)
* Fermer un ticket par le message de commit (ex. : `Correction orthographique. fixes #123`) [mais aussi *fix*, *close*, *resolve*, *implement*...](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html)

Note: fermer un ticket depuis le message de commit n'est possible que depuis la branche *main*.
