# Session 2 : GitLab et dépôt distant
#### Pratiquer
* GitLab :
    * Présentation générale
    * Configuration des accès
    * Présentations des *issues*
* Interactions avec un dépôt distant et son dépôt local

Note: Pas de collaboration dans cette section avec Git
