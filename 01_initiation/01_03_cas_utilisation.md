---slide---

# Cas d'utilisation

- Modifier son code et vouloir faire marche arrière
- Maintenir plusieurs versions du code
- Comparer 2 versions du même code
- Suivre les changements sur mes fichiers
- Parcourir l'historique d'un bout de code

Note: Cas d'utilisations abordés dans cette session
