---slide---

# États (session 1)

<div class="container">
<img src='images/etats_session1.png'/>
</div>
<div class="col">

* *Working directory* : répertoire de travail
* *Stage* : index pour tracer les modifications
* *Repo* : ensemble des fichiers (les vôtres et ceux de git)
* *HEAD* : pointeur identifiant le dernier commit
</div>
Note: mise en évidence des états concernés lors de cette session.
Il faudrait avoir une définition plus précise de HEAD. Un truc concret

