---slide---

# Première pratique en local (VS Code)


---vertical---

# Installation VS Code

* tous OS, une seule page : <https://code.visualstudio.com/download>

* Installer l'extension git-history

---vertical---

# Démonstration des opérations de base avec VS Code

Prérequis : disposer d'un répertoire dans lequel on peut écrire

---vertical---

Démarrage de VS Code sur un dossier vierge

![Démarrer VS Code](images/05_vscode_demarrage.png)

---vertical---

Démarrage de VS Code sur un dossier existant

![Démarrer VS Code](images/05_vscode_demarrage.png)
---vertical---

# Initialisation d’un dépôt Git (VS Code)
---vertical---
Dans VS Code, sur le dossier à suivre

Ouvrir l'outil "Source Control"

![source control](images/05_vscode_source_control.png)


---vertical---
Cliquer sur le bouton "Initialize repository"

![Initialiser un dépôt Git "local" pour suivre les fichiers du dossier](images/05_vscode_init_dossier.png)

---vertical---
Un dossier `.git` est créé

C'est là que Git stocke les informations dont il a besoin.



---vertical---
Contrôler en ouvrant un terminal et en vérifiant la présence d'un dossier ".git"

![dossier .git](images/05_vscode_.git.png)

---vertical---

![interdiction](images/interdit.png)

Ne pas essayer d'éditer les fichiers du dossier .git



---vertical---
# Consultation état du dépôt
---vertical---
L'outils "Source Control" permet de connaître l'état du dépôt rattaché au dossier :

![VS Code états Git](images/05_vscode_etats_git.png)
---vertical---
Création d'un fichier `.gitignore`


![création .gitignore](images/05_vscode_creation_gitignore.png)

---vertical---
Le fichier `.gitignore` permet de préciser quels fichiers doivent être ignorés par Git.

Editez le fichier pour qu'il contienne la ligne ".Rproj"

Ainsi tous les fichiers ayant cette extension seront ignorés
---vertical---

On constate que le fichier .gitignore est noté "U" pour untracked

![équivalence git status](images/05_vscode_status.png)



---vertical---

# Ajouter des fichiers et leurs modifications sur le dépôt (VS Code)

Créer des commits
---vertical---

On va déclencher des opérations de "commit" pour :

- ajouter sur le dépôt des fichiers qui ne sont pas encore suivis
- enregistrer des modifications réalisées sur des fichiers suivis

Chaque "commit" dispose d'un identifiant unique, géré par Git.

---vertical---

Pour créer un commit, il faut commencer par avoir quelque chose à "commiter".
On va donc créer un fichier.

![Création fichier](images/05_vscode_creation_fichier.png)

Constatez que le fichier est proposé en vert (add) dans "Source Control"
---vertical---

Sélection des fichiers à commiter

![Equivalence git add](images/05_vscode_add_automatique.png)

* survol puis "+" pour le passer dans la zone "staged"
* ou prendre toutes les modifications (clic sur le "+" à côté de "changes")


---vertical---

Finalement, on déclenche l'opération de commit (2), après avoir saisi un message d'accompagnement (1) puis "Ctrl Enter" ou on clique sur la case à cocher dans l'onglet "Source control"

![commit](images/05_vscode_commit.png)

A ce stade, le fichier mon_fichier.txt est enregistré dans les fichiers suivis par Git.

---vertical---

Si une erreur survient avec le message suivant :

```text
Please tell me who you are
```

C'est que votre identité n'est pas connue de Git (les commit sont signés par des auteurs).
Utilisez alors les deux commandes suivantes pour configurer Git (à faire une fois) :

```bash
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
```

On peut ensuite relancer le commit


---vertical---





---vertical---

Pensez à saisir un message pertinent !

![commit .gitignore](images/05_vscode_commit_gitignore.png)


---vertical---
On peut alors vérifier qu'après l'ajout d'un fichier avec l'extension ".Rproj", Git ne propose pas de le suivre.

![Rproj ignoré](images/05_vscode_Rproj_ignore.png)


---vertical---
Si plusieurs fichiers ont été modifiés, au moment du commit, VS Code le signale.

![commit incomplet](images/05_vscode_commit_incomplet.png)
---vertical---
---vertical---
# Bonnes pratiques (commit)
---vertical---
Bonne pratique pour les commits :
- À intervalle régulier
- Approche "atomique"
- Un changement dans le code = un commit
- Mauvaise pratique : un commit en fin de journée

---vertical---

Messages de commit :
- Commentaires courts, informatifs et explicites sur le changement présents dans le commit
- Mauvaise pratique : `Update code.r`
- Mauvaise pratique : `Petit rajout`
- Mauvaise pratique : `Code réparé`
---vertical---
# Historique du dépot (VS Code)
---vertical---

Exercice: prendre le temps de faire quelques commits supplémentaires en ajoutant/modifiant des fichiers sur le dépôt
---vertical---

Pour voir l'historique des commits sur un dépôt

![repo history](images/05_vscode_repo_history.png)

Nécessite d'avoir l'extension "Git history" installée

---vertical---
En cliquant sur une ligne de l'historique on peut accéder aux détails du commit

![détail historique](images/05_vscode_detail_history.png)


---vertical---
![boutons historique](images/05_vscode_boutons_history.png)

* (1) Visualiser le fichier dans l'état du commit
* (2) Comparer le fichier avec celui dans la working copy
* Comparer le fichier avec la version du commit précédent
* (3) Retourner à l'historique complet sur ce fichier

---vertical---

Git dans VS Code permet aussi aussi de suivre les suppressions et renommages de fichiers

![suppression fichier](images/05_vscode_suppression_fichier.png)


---vertical---

---vertical---

---vertical---

# Annuler des modifications non commitées

---vertical---

Modifier un fichier sans faire de commit sur les modifications

![Fichier avec quelques lignes en plus](images/05_vscode_modification_fichier.png)

Pensez à sauvegarder le fichier

---vertical---

On peut aller dans l'interface de commit pour voir les modifications :

![Vue des modifications dans l'interface de commit](images/05_vscode_vue_modification.png)

---vertical---

Si l'on souhaite supprimer **toutes** les modifications non commitées, on peut cliquer sur la flèche "discard changes" pour le fichier concerné:

![Clic droit sur un fichier en vue d'un revert](images/05_vscode_clic_revert.png)

---vertical---

Attention, c'est tout ce qui a été modifié dans le fichier depuis le dernier commit qui sera supprimé :

![Clic droit sur un fichier en vue d'un revert](images/05_vscode_annulation_modifications.png)

---vertical---
---vertical---
# Grouper un commit avec un commit précédent
---vertical---
Ajouter les nouvelles modifications au commit précédent en passant par le terminal puis
```bash
git commit -m "groupement de commit" --amend
```
Constatez qu'il n'y a pas de nouveau commit dans la liste dans l'historique.
