---slide---

# Première pratique en local (ligne de commande)

---vertical---

# Installation de Git ligne de commande

* Windows : <https://github.com/git-for-windows/git/releases/tag/v2.39.1.windows.1>
* Linux : `sudo apt-get install git`
* Mac : <https://git-scm.com/download/mac>


---vertical---

# Démonstration des opérations de base en ligne de commande

Prérequis : disposer d'un répertoire dans lequel on peut écrire

---vertical---

Pour un dossier vide

```{bash}
$ cd; mkdir mon_projet
```
Un dossier "mon_projet" est créé.

---vertical---

Pour un dossier contenant déjà des fichiers

```{bash}
$ cd mon_projet
```

---vertical---
# Initialisation d’un dépôt Git (ligne de commande)
---vertical---

Initialiser un dépôt Git "local" pour suivre les fichiers du dossier "mon_projet":

```{bash}
$ git init .
```
---vertical---
Un dossier `.git` est créé dans mon_projet.

C'est là que Git stocke les informations dont il a besoin

---vertical---
Consulter son contenu dans un terminal

![.git](images/05_lc_.git.png)

---vertical---

![interdiction](images/interdit.png)

Ne pas essayer d'éditer les fichiers du dossier `.git`.

---vertical---
---vertical---
# Consultation état du dépôt
---vertical---
Pour vérifier l'état du dépot :
```{bash}
$ git status
```
---vertical---
![lc etats git](images/05_lc_etats_git.png)

On obtient la liste
*  des nouveaux fichiers (**U**ntracked)
*  des fichiers modifiés  (**M**odified)

---vertical---
On ajoute un fichier `.gitignore` qui permet de préciser les fichiers qu'il ne faut pas suivre
```{bash}
$ touch .gitignore
```
---vertical---
Editez le fichier pour qu'il contienne la ligne **.Rproj"

Ainsi tous les fichiers ayant cette extension seront ignorés

![cat .gitignore](images/05_lc_cat_gitignore.png)
Voyons comment l'ajouter au dépôt avec un commit !
---vertical---
# Ajouter des fichiers et leurs modifications sur le dépôt (LC)

Créer des commits
---vertical---

Un commit permet :

- D'ajouter sur le dépôt des fichiers qui ne sont pas encore suivis
- D'enregistrer des modifications réalisées sur des fichiers suivis

Chaque "commit" dispose d'un identifiant unique, géré par Git.

---vertical---

Pour créer un commit, il faut commencer par avoir quelque chose à commiter
On va donc créer un fichier

```{bash}
# Création d'un fichier
$ touch mon_fichier.txt
```
---vertical---
Ensuite, il faut désigner les fichiers qui vont faire l'objet du commit

```{bash}
# Pour prendre en compte tous les fichiers présents
$ git add .
```

---vertical---
OU

```{bash}
# Pour ne prendre en compte que le fichier mon_fichier.txt
$ git add  mon_fichier.txt
```

---vertical---

Finalement, on déclenche l'opération de commit, avec un message pertinent:

```{bash}
$ git commit -m "Mon premier commit !"
```

À ce stade, le fichier mon_fichier.txt est enregistré dans les fichiers suivis par Git.

---vertical---

Si une erreur survient avec le message suivant :

```text
Please tell me who you are
```

C'est que votre identité n'est pas connue de Git (les commit sont signés par des auteurs).
Utilisez alors les deux commandes suivantes pour configurer Git (à faire une fois) :

```bash
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
```

On peut ensuite relancer le commit

```{bash}
$ git commit -m "Mon premier commit !"
```

---vertical---


Nous allons modifier le fichier `.gitignore` pour qu'il ignore les fichiers ayant l'extension `.Rproj`.

Pour cela, ouvrir le fichier `.gitignore` et ajouter la ligne suivante :


```text
*.Rproj
```


---vertical---

On constate que le fichier .gitignore a été  mis à jour :

Nous allons faire un nouveau commit pour ajouter ces modifications au dépôt

---vertical---

```{bash}
$ git commit -m "mise à jour .gitignore pour les fichiers d'extension .Rproj"
$ git status
```
Le dépot est annoncé comme étant propre: pas de modifications à prendre en compte !
---vertical---
On peut alors vérifier qu'après l'ajout d'un fichier avec l'extension ".Rproj", git ne propose pas de le suivre
```{bash}
#création d'un fichier avec l'extension non gérée
$ touch test.Rproj
$ git status
```
---vertical---
# Bonnes pratiques (commit)
---vertical---
Bonne pratique pour les commits :
- À intervalle régulier
- Approche "atomique"
- Un changement dans le code = un commit
- Mauvaise pratique : un commit en fin de journée

---vertical---

Bonnes propriétés pour les messages de commit :
- Commentaires courts, informatifs et explicites sur le changement présents dans le commit
- Mauvaise pratique : `Update code.r`
- Mauvaise pratique : `Petit rajout`
- Mauvaise pratique : `Code réparé`

---vertical---
# Historique du dépot (LC)
---vertical---

Exercice : prendre le temps de faire quelques commits supplémentaires en ajoutant/modifiant des fichiers sur le dépôt

---vertical---
Pour voir l'historique des commits sur un dépôt, on peut utiliser les commandes suivantes :

```bash
# Affiche l'historique des commits, avec un hash pour chacun
git log
```
---vertical---
![git log](images/04_lc_git_log.png)
---vertical---
On peut afficher les modifications du commit correspondant à un hash particulier
```bash
git show <hash du commit intéressant>
```

![git show](images/04_lc_git_show.png)

---vertical---

Git permet aussi aussi de suivre les suppressions et renommages de fichiers :

```bash
#Déplacer un fichier
$ git mv fichier.txt new_name.txt
$ git commit -m "Mon message de commit"
#Supprimer un fichier
$ git rm fichier.txt new_name.txt
$ git commit -m "Mon message de commit"
```
---vertical---

Pour annuler les modifications non commitées sur un fichier, on peut utiliser la commande suivante :

```bash
git restore <nom du fichier>
```
---vertical---

---vertical---

# Annuler des modifications non commitées
---vertical---

Modifier un fichier sans faire de commit sur les modifications

```nano mon_fichier.txt```
Pensez à sauvegarder le fichier

---vertical---

Voir les différences avec diff

```bash
git diff
```

---vertical---

Pour annuler les modifications non commitées et indexées.

```bash
git reset -- mon_fichier.txt
```

---vertical---

Pour annuler les modifications non commitées et non indexées (retrouver le fichier de l'index ou d'un autre commit).

```bash
git checkout -- mon_fichier.txt
# ou
git restore mon_fichier.txt
```

Note:
- [Reset, restore and revert](https://git-scm.com/docs/git#_reset_restore_and_revert)
- pour `checkout` : https://git-scm.com/book/fr/v2/Les-bases-de-Git-Annuler-des-actions#_r%C3%A9initialiser_un_fichier_modifi%C3%A9
- pour `restore` : https://git-scm.com/book/fr/v2/Les-bases-de-Git-Annuler-des-actions#undoing_git_restore

---vertical---

Pour désindexer un fichier modifié et indexé.

```bash
git restore --staged mon_fichier.txt
```

Note:
Désindexer (*unstage*) c'est passer de l'indexe (*Staging area*) ([diapo 3](https://git-gitlab-paca.pages.mia.inra.fr/support-de-formation/01_initiation/#/3)) au dossier de travail (*Working directory*), soit l'inverse de `git add`. Il n'y a pas encore eu de commit.

---vertical---

# Grouper un commit avec un commit précédent

---vertical---

Ajouter les nouvelles modifications au commit précédent avec
```bash
git commit -m "groupement de commit" --amend
```
Constatez qu'il n'y a pas de nouveau commit dans la liste dans l'historique
