---slide---

# Étiquetage
## avec VS Code

NB : la traduction en français dans VS Code de *tag* est « étiquette » ou « balise ».

---vertical---

### Lister les étiquettes existantes

Dans le menu, cliquer sur *Basculer sur...*.

![Menu](images/06_03_tag_vscode_basculer.png)

---vertical---

### Rechercher les étiquettes correspondant à un motif particulier

![Menu](images/06_03_tag_vscode_basculer2.png)

---vertical---

### Créer une étiquette légère

VS Code ne propose que des étiquettes annotées.

Solution : utiliser la ligne de commande.

---vertical---

### Créer une étiquette annotée

Dans le menu, cliquer sur *Étiquettes* > *Créer une balise*.

![Menu](images/06_03_tag_vscode_menu.png)

---vertical---

### Étiqueter après coup

VS Code ne propose pas d'étiqueter à partir d'un *commit* antérieur.

Solution : utiliser la ligne de commande.

---vertical---

### Visualiser les données d'une étiquette légère

VS Code ne propose pas de visualiser les données d'une étiquette.

Solution : utiliser la ligne de commande.

---vertical---

### Visualiser les données d'une étiquette annotée

VS Code ne propose pas de visualiser les données d'une étiquette.

Solution : utiliser la ligne de commande.

---vertical---

### Supprimer une étiquette

Dans le menu, cliquer sur *Étiquettes* > *Supprimer l'étiquette*.

![Menu](images/06_03_tag_vscode_menu.png)

---vertical---

### Extraire une étiquette

Après avoir cliqué dans le menu sur *Basculer sur...*, choisir l'étiquette.

![Menu](images/06_03_tag_vscode_basculer2.png)
