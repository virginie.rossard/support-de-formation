---slide---

# Présentation de Git

![](../images/logo_git.png)

* Créé en 2005 par Linus Torvalds (créateur de Linux).
* Logiciel de contrôle de version open source.
* Principale tâche : gérer l'évolution du contenu d'une arborescence.
* Gère les différents états d'un code au cours du temps.

---vertical---

# Comment utiliser Git ?

* Logiciel client à installer sur le poste (installateur Git).
* Interaction par une interface graphique ou en ligne de commande.
* Git intégré ou intégrable aux IDE (Eclipse, RStudio, VS Code...).

---vertical---
# Du code ?

![code](images/formation_git_code_texte.png)

Note:
Le suivi est facilité pour les fichiers au format texte (code source, fichier Markdown, LaTex...).
