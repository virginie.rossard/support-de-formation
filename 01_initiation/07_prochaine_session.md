---slide---
# Prochaine session

---vertical---

- Premiers pas avec GitLab
- Travailler avec un dépôt Git distant (sur un serveur)
- Proposer un changement au code de quelqu'un d'autre
- Projet GitLab et *issues*
- Bonnes pratiques

Note: cas d'utilisation suivant le public que l'on veut cibler. Les cas d'utilisation montreront les chapitres à aborder.
Aguiche pour le courriel annonçant la formation.
