---slide---

# Étiquetage
## en ligne de commande

---vertical---

### Lister les étiquettes existantes

```sh
$ git tag
v0.1
v1.3
```

=> liste les étiquettes dans l’ordre alphabétique

---vertical---

### Rechercher les étiquettes correspondant à un motif particulier

```sh
$ git tag -l 'v1.*'
v1.3
```

---vertical---

### Créer une étiquette légère

```
$ git tag v1.4-lg
$ git tag
v0.1
v1.3
v1.4-lg
```

---vertical---

### Créer une étiquette annotée

```sh
$ git tag -a v1.4 -m 'ma version 1.4'
$ git tag
v0.1
v1.3
v1.4
```

---vertical---

### Étiqueter après coup

Vous pouvez aussi étiqueter des commits plus anciens.

```sh
$ git tag -a v1.2 9fceb02
```

---vertical---

### Visualiser les données d'une étiquette légère

```sh
$ git show v1.4-lg
commit ca82a6dff817ec66f44342007202690a93763949
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the version number
```

---vertical---

### Visualiser les données d'une étiquette annotée

```sh
$ git show v1.4
tag v1.4
Tagger: Ben Straub <ben@straub.cc>
Date:   Sat May 3 20:19:12 2014 -0700

ma version 1.4

commit ca82a6dff817ec66f44342007202690a93763949
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the version number
```

---vertical---

### Supprimer une étiquette

```sh
$ git tag -d v1.4-lw
Deleted tag 'v1.4-lw' (was e7d5add)
```

---vertical---

### Extraire une étiquette

Pour voir les versions de fichiers qu’une étiquette pointe.

```sh
$ git checkout v2.29.2
```

/!\ votre dépôt sera dans un état « HEAD détachée ».

Si vous modifiez puis créez un commit, l’étiquette restera identique, mais votre nouveau commit n’appartiendra à aucune branche et sera non joignable, à part avec son empreinte de commit exacte.

=> créer une branche (voir dans une prochaine session)
```

