---slide---

# Première pratique en local (RStudio)

---vertical---
# Installation RStudio

* Windows : <https://git-scm.com/download/win>
* Linux : `sudo apt-get install git`
* Mac : <https://git-scm.com/download/mac>
* Interface client (optionnelle) :
	* [tortoisegit](https://tortoisegit.org/)


---vertical---

# Démonstration des opérations de base avec RStudio

Prérequis : travailler dans un projet [RStudio](https://posit.co/download/rstudio-desktop/)

[Documentation RStudio](https://support.posit.co/hc/en-us/articles/200532077)

---vertical---

Créer un projet

![Créer un projet](images/03_02_00_demo_creation_projet.png)

---vertical---

Ouvrir un projet existant

![Ouvrir un projet existant](images/03_02_00_demo_ouverture_projet.png)

---vertical---

# Initialisation d’un dépôt git (RStudio)

---vertical---

Lors de la création d'un projet RStudio

![Initialiser un dépôt lors de la création d'un projet RStudio](images/03_02_01_init_avec_un_projet_rstudio.png)

---vertical---

Dans un projet existant (1/3)

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant.png)

---vertical---

Dans un projet existant (2/3)

Choisir "Git" dans le menu "Version control system"

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant2.png)

---vertical---

Dans un projet existant (3/3)

![Initialiser un dépôt dans un projet existant](images/03_02_01_init_projet_existant3.png)

---vertical---

Git est désormais intégré au projet :

![Vue git dans RStudio](images/03_02_01_init_resultat.png)

---vertical---
# Consultation état du dépôt
---vertical---

L'onglet Git de RStudio vous permet de connaître l'état du dépôt rattaché à votre projet :

![Onglet git dans RStudio](images/03_02_01_onglet_git.png)

---vertical---

On voit dans l'interface que le fichier `.gitignore` n'est pas encore suivi par Git : icône orange avec un "`?`" et état "untracked"

![État du gitignore après initialisation dans RStudio](images/03_02_01_etat_gitignore.png)

---vertical---

Voir à droite pour l'équivalent en ligne de commande

``` git status ```

---vertical---

Le fichier `.gitignore` permet de préciser quels fichiers doivent être ignorés par Git.
Par défaut, il mentionne les fichiers suivants :
```
.Rproj.user
.Rhistory
.RData
.Ruserdata
```

Voyons comment l'ajouter au dépôt avec un commit !

---vertical---

# Ajouter des fichiers et leurs modifications sur le dépôt (RStudio)

Créer des commits

---vertical---

Un commit permet :

- D'ajouter sur le dépôt des fichiers qui ne sont pas encore suivis
- D'enregistrer des modifications réalisées sur des fichiers suivis

Chaque "commit" dispose d'un identifiant unique, géré par Git.
---vertical---

Pour créer un commit, cliquer sur le bouton "Commit" :

![Bouton de commit de l'interface RStudio](images/03_02_03_bouton_commit.png)

Il existe aussi le raccourci clavier : `Ctrl+Alt+M`

---vertical---

Une fenêtre s'ouvre permettant de voir les fichiers non suivis ou les fichiers avec des modifications :

![Interface de commit](images/03_02_03_interface_commit.png)

---vertical---

Pour créer un commit contenant le fichier `.gitignore` :
1. Sélectionner le fichier `.gitignore`
1. Écrire un message de commit
1. Cliquer sur le bouton "Commit"

![Ajouter le gitignore au dépôt](images/03_02_03_ajouter_gitignore.png)

---vertical---

Si une erreur survient avec le message suivant :

```text
Please tell me who you are

…
```

C'est que votre identité n'est pas connue de git (les commit sont signés par des auteurs).
Aller dans le terminal de la fenêtre RStudio et utilisez les deux commandes suivantes pour configurer Git :

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

---vertical---

![Configuration du nom et du mail dans git](images/03_02_03_configuration_git.png)

On peut ensuite re-tenter de créer le commit dans la fenêtre de commit

---vertical---

Si tout se passe bien, on obtient un message ressemblant à celui-ci :

![Vue de l'interface suite à un commit réussi](images/03_02_03_commit_succes.png)

On peut alors fermer la fenêtre de commit

---vertical---

Nous allons modifier le fichier `.gitignore` pour qu'il ignore les fichiers ayant l'extension `.Rproj`.

Pour cela, ouvrir le fichier `.gitignore` et ajouter la ligne suivante :


```text
*.Rproj
```

![Modification du fichier gitignore](images/03_02_03_modification_gitignore.png)

---vertical---

On constate que l'onglet dépôt a été mis à jour :
- Le fichier `.Rproj` du projet a disparu
- Le fichier `.gitignore` a été modifié (petite icône bleue avec un "M" à l'intérieur)

![Nouvel état du dépôt](images/03_02_03_nouvel_etat_depot.png)

Nous allons faire un nouveau commit pour ajouter ces modifications au dépôt

---vertical---

Cliquer sur le bouton "Commit" et faire un commit pour cette modification

![Nouveau commit pour les modifications du gitignore](images/03_02_03_second_commit.png)

---vertical---
# Bonnes pratiques (commit)
---vertical---
Bonne pratique pour les commits :
- À intervalle régulier
- Approche "atomique"
- Un changement dans le code = un commit
- Mauvaise pratique : un commit en fin de journée

---vertical---

Messages de commit :
- Commentaires courts, informatifs et explicites sur le changement présents dans le commit
- Mauvaise pratique : `Update code.r`
- Mauvaise pratique : `Petit rajout`
- Mauvaise pratique : `Code réparé`

---vertical---

# Historique du dépot (RStudio)

---vertical---

Exercice : prendre le temps de faire quelques commits supplémentaires en ajoutant/modifiant des fichiers sur le dépôt

---vertical---

Dans l'onglet git de RStudio, il y a un bouton "History" :

![Bouton history de l'interface RStudio](images/03_02_04_bouton_history.png)

---vertical---

Ce bouton permet d'accéder à l'historique du dépôt (tous les commits effectués dessus) :

![Historique du dépôt dans l'interface dédiée](images/03_02_04_history.png)

---vertical---

En sélectionnant un commit, on peut voir les informations qui le concernent :

![Vue d'un commit dans l'interface dédiée](images/03_02_04_vue_commit.png)

---vertical---

Il est aussi possible de voir un fichier tel qu'il était suite à un commit particulier :

![Bouton pour voir un fichier d'après un commit](images/03_02_04_vue_fichier_commit_bouton.png)

---vertical---

On peut alors parcourir un "instantané" du fichier :

![Vue d'un fichier d'après un commit](images/03_02_04_vue_fichier_commit.png)

---vertical---

Si l'on souhaite récupérer cet instantané, on peut l'enregistrer comme fichier :

![Enregistrer un fichier correspondant à un fichier particulier](images/03_02_04_enregistrer_fichier_commit.png)

---vertical---

# Annuler des modifications non commitées

---vertical---

Modifier un fichier sans faire de commit sur les modifications :

![Fichier avec quelques lignes en plus](images/03_02_05_modification_fichier.png)

---vertical---

On peut aller dans l'interface de commit pour voir les modifications :

![Vue des modifications dans l'interface de commit](images/03_02_05_vue_modifications.png)

---vertical---

Si l'on souhaite supprimer **toutes** les modifications non commitées, on peut faire un clic-droit sur le fichier concerné et faire un "revert" :

![Clic droit sur un fichier en vue d'un revert](images/03_02_05_revert.png)

---vertical---

Attention, c'est tout ce qui a été modifié dans le fichier depuis le dernier commit qui sera supprimé :

![Clic droit sur un fichier en vue d'un revert](images/03_02_05_revert_warning.png)

---vertical---

Pour annuler les modifications non commitées sur un fichier, on peut utiliser la commande suivante :

```bash
git restore <nom du fichier>
```

---vertical---

# Grouper un commit avec un commit précédent

---vertical---

Ajouter les nouvelles modifications au commit précédent en ouvrant le terminal puis
```bash
git commit -m "groupement de commit" --amend
```
Constatez qu'il n'y a pas de nouveau commit dans la liste dans l'historique
