---slide---

# États

Les états du système de fichiers
<div class="container">
<img src='images/etats.png'/>
<p>INFO : Les modifications locales sont stockées dans le dossier `.git/`.</p>

</div>

Note: Faire un schéma simplifié working staging local repo, sans parler de remote. cf 02_02_etats-session1_git.md
Cela permet d'aborder les opérations de base en abordant l'historique, différences...
C'est le répertoire du système de fichiers sur lequel on travaille, les fichiers dans cette zone sont *inconnus* de Git. Ils sont dit "non suivis" ou `untracked file`
