---slide---

# À quoi sert Git ?

* Suivi des changements dans le code
* Mise à disposition / partage
* Mise en place des outillages logiciels (vérifications automatiques, empaquetage...)

Note:
Maintenir, organiser et développer un logiciel d'une certaine taille nécessite la mise
en œuvre de bonnes pratiques. Les évolutions du code source doivent être tracées
avec un système de contrôle de version.
De nombreuses plateformes web, appelées forges logicielles (bitbucket.org,
GitLab, GitHub, etc.), simplifient ces tâches et facilitent l’agrégation de
communautés de contributeurs.
Les communautés de chaque plateforme peuvent apporter des bénéfices
appréciables : recherche de bogues, ajout de nouvelles fonctionnalités,
pérennisation du logiciel, gestion des contributeurs et de leurs permissions, mise
en place de tests de qualité, etc. Pour bénéficier de l’apport d’une communauté,
il faut investir le temps et l’énergie nécessaires pour organiser son fonctionnement :
évaluer et gérer rapidement les suggestions et contributions apportées, planifier la
feuille de route du développement logiciel..
Les forges logicielles ne sont pas des archives pérennes : les projets
qu’elles contiennent peuvent être modifiés ou effacés. Des forges
entières peuvent aussi disparaître, comme ce fut le cas de Google Code
et Gitorious en 2015 et d’une partie de BitBucket en 2020. Il est donc préférable de
choisir une forge gérée par une organisation publique et recommandée par votre
institution et de vous assurer de l'archivage de vos codes sur le long terme dans
des archives dédiées aux logiciels, comme Software Heritage.
<https://www.ouvrirlascience.fr/wp-content/uploads/2022/10/Passeport_Codes-et-logiciels_WEB.pdf>

---vertical---

# Bénéfices en général

* Traçabilité des modifications
* Retour à une version antérieure
* Synchronisation entre plusieurs postes
* Copie de travail locale (pour un travail hors ligne)
* Travail sur des questions en parallèle
