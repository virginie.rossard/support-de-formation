---slide---

# Étiquetage

## *tag* en anglais

Fonctionnalité de Git : étiqueter un certain état dans l’historique comme important.

Ex. : marquer les états de publication (v1.0 et ainsi de suite)

---vertical---

## Les différents types d’étiquettes.

Deux types principaux d’étiquettes :

* légères
* annotées

---vertical---

### Étiquettes légères

Un pointeur sur un *commit* spécifique.

Suffit si aucune information supplémentaire n’est pas désirée ou l'étiquette doit rester temporaire.

---vertical---

### Étiquettes annotées

objets à part entière dans la base de données de Git :

* une somme de contrôle,
* le nom et l’adresse courriel du créateur,
* la date,
* un message d’étiquetage,
* peuvent être signées et vérifiées avec GNU Privacy Guard (GPG).

Recommandées pour générer toute cette information.

---vertical---

Dans cette partie :

* lister les différentes étiquettes,
* créer de nouvelles étiquettes,
* visualiser les données d'une étiquette,
* supprimer une étiquette.
