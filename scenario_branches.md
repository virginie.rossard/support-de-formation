# Scénario Gitlab « branches »

## Action Formateur (à faire avant la formation)

### Création d'une copie du projet exercice_session3
* Aller à la page [du projet exercice_session3](https://forgemia.inra.fr:git-gitlab-paca/exercice_session3)
* Récupération du code (tgz) projet `git@forgemia.inra.fr:git-gitlab-paca/exercice_session3.git` et installation dans un nouveau répertoire local

* Création d'un nouveau projet dans un autre espace de nom (au choix)
  * Nouveau nom du projet: session3_labo
  * Description: démo session 3, pour groupe ... , date ...
  * Visibilité: private
  * casser le lien avec le projet source (fork relationship / relation de bifurcation) via general settings/ Advanced/ Fork relationship

### Configuration du nouveau projet
* Déclarer les membres en fonction des présents (Manage/Members), en rôle "developper"
* configuration à faire pour empêcher la modification de la branche "main" par les stagiaires:       
    * bloquer qui peut modifier la branche main (settings/Merge request,Merge commit with semi-linear history )
    * repository / protected branch et sur main, bloquer les push (no one )

# But de l'exercice
A la place d'un code, on a mis du texte faisant référence à des évènements de l'histoire de France.

Des erreurs ont été faites volontairement.

Le jeu sera de les corriger, tout en adoptant les bonnes pratiques concernant l'usage des branches

La bonne pratique est ici de créer une issue et à partir de cette issue créer une branche dans laquelle on va faire la correction. A la fin, il y a autant de branches que d'issues. Chaque branche avec sa correction est ensuite fusionnée dans la branche principale, nommée main

De manière générale, on rappelle que la branche main doit être la branche stable, c'est à dire celle qui porte le code le plus à jour et opérationnel.

## Les issues

Le formateur créé les issues en direct, en utilisant les templates fournis, qui contiennent les corrigés.

Les templates sont nommés en fonction du fichier et du chapitre concerné.


## Tous les utilisateurs
* pull du projet "fork"
* Découverte des fichiers

## Par le formateur
* Affectation des issues
* Au passage, expliquer qu'il joue le rôle de chef de projet
* Il explique les règles de MR (pas sur main) et que ce sont des conventions à définir/appliquer dès le début du projet
* On choisi le modèle une branche main stable, des branches par issues pour faire évoluer le code.


# Use case idéal

## Utilisateur en charge de l'issue Partie1_Chapitre1, les autres ne font rien
* ouverture de l'issue
* échange éventuel de commentaires avec le commanditaire de l'issue (formateur)
* création d'une MR et d'une branche sur la forge

* récupération de la nouvelle branche, en local
* Correction de l'erreur dans le fichier texte en question
* commit
* push
* Connexion à la forge
* Activation de la demande de fusion

## Tous les utilisateurs
* observent les mouvements sur la forge, les changements proposés dans le texte
* peuvent accepter la fusion

## Le formateur
* montre le contenu de la fusion, les changements , les commits associés
* procède à une revue de code sur les changements proposés (option)
* valide la fusion
* déclenche la fusion

## Tous les utilisateurs
* pull --> mise à jour des dépôts locaux
* A ce stade, tout le monde est synchro (même dépôt pour tous)

# Use case rebasage nécessaire
Deux utilisateurs vont faire des corrections sur des fichiers différents, mais la deuxième fusion va se retrouver bloquée parce que la branche à fusionner sera en retard par rapport au main.

## Utilisateurs en charge des issues Partie2_Chapitre et 3, les autres ne font rien
* ouverture de l'issue
* échange éventuel de commentaires
* création d'une MR et d'une branche sur la forge

* récupération de la nouvelle branche, en local
* Correction de l'erreur dans le fichier texte en question
* éventuellement dialogue avec celui qui a signalé l'issue
* commit
* push
* Connexion à la forge
* Activation de la demande de fusion

## Formateur
* Constat que la troisième fusion ne peut être faite parce que le dépôt n'est plus à jour (il ne contient pas la fusion 2 par exemple)

* résolution du problème par rebasage (on embarque dans la branche locale )
```bash
git checkout issue3
git rebase origin/main
```

# Use case conflit
Deux utilisateurs vont modifier la même partie d'un fichier. git ne pourra pas arbitrer par lui même. On est dans un situation dite de conflit.

## Utilisateur en charge issue Partie2_Chapitre2
* ouverture de l'issue
* échange éventuel de commentaires
* création d'une MR et d'une branche sur la forge

* récupération de la nouvelle branche, en local
* Correction de l'erreur dans le fichier texte en question
* éventuellement dialogue avec celui qui a signalé l'issue
* commit
* push
* Connexion à la forge
* Activation de la demande de fusion

## Formateur
* Prise en charge de l'issue Partie2_Chapitre2_bis (création branche à faire avant le push de la branche de l'issue précédente)
* Activation de la fusion précédente sans pull (en local, main est en retard, ainsi que la branche de l'issue)
* récupération de la nouvelle branche en local
* Correction de l'erreur dans le même fichier texte
* commit
* push
* Connexion à la forge
* Activation de la demande de fusion
* Constat de l'impossibilité à cause d'un conflit (git ne sait pas choisir laquelle des deux versions conserver)
* On peut voir les marques du conflit depuis les outils de la forge
* On peut aussi (bonne pratique) gérer ce conflit localement

```bash
git switch Partie2_Chapitre2_bis
git fetch
git rebase origin/main
```
* lire les messages  retournés par git !!
* ouvrir le fichier en conflit dans un éditeur
* constater les marques de conflit
* corriger (choisir le code à conserver, supprimer l'autre, supprimer les marques de conflit)

```bash
git add .
git commit -m "Fix conflict on issue Partie2_Chapitre2_bis"
git rebase --continue
```