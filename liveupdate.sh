#!/bin/bash
 
if [ -z "$(which inotifywait)" ]; then
    echo "inotifywait not installed."
    echo "In most distros, it is available in the inotify-tools package."
    exit 1
fi

if [ "$1" == "" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

if [ ! -d $1 ]; then
    echo "$1 is not a directory."
    exit 1
fi

echo "Update changes in $1."

BUILD_DIR=public/$1
SLIDES_MD=$BUILD_DIR/slides.md

if [ ! -d $BUILD_DIR ]; then
    echo "Create $BUILD_DIR"
    mkdir -p $BUILD_DIR
fi

make build
(cd public/ ; python3 -m http.server) &
python3 -mwebbrowser http://0.0.0.0:8000/$1 

inotifywait --recursive --monitor --format "%e %w%f" --event modify,move,create,delete $1 | while read changed; do
    echo $changed
    cat $1/*md > $SLIDES_MD
done