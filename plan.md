# Plan de la formation

## [Session d'initiation](01_initiation/)

- 1. [Introduction](01_initiation/01_00_introduction.md)
  - 1.1. [Présentation de Git](01_initiation/01_01_presentation_git.md)
  - 1.2. [Bénéfices de l'usage de Git](01_initiation/01_02_benefices_git.md)
  - 1.3. [Cas d’utilisation](01_initiation/01_03_cas_utilisation.md)
- 2. [États de Git en local](01_initiation/02_00_etats_git.md)
  - 2.1. [Cas session1](01_initiation/02_01_etats-session1_git.md)
- 3. [Première pratique en local - RStudio](01_initiation/03_rstudio_complet.md)
- 4. [Première pratique en local - Ligne de commande](01_initiation/04_ligne de commande_complet.md)
- 5. [Première pratique en local - VSCode](01_initiation/05_vscode_complet.md)
- 6. [Étiquettes](01_initiation/06_00_tag.md)
  - 6.1. [Étiquettes dans RStudio](01_initiation/06_01_tag_rstudio.md)
  - 6.2. [Étiquettes en ligne de commande](01_initiation/06_02_tag_ligne_de_commande.md)
  - 6.3. [Étiquettes dans VSCode](01_initiation/06_03_tag_vscode.md)
- 7. [Prochaine session](01_initiation/07_prochaine_session.md)

## [GitLab et dépôts distants](02_gitlab_depot_distant/)

- 1. [Intro](02_gitlab_depot_distant/01_00_introduction.md)
- 2. [Rappels](02_gitlab_depot_distant/02_00_rappels.md)
- 3. [Diagramme d'état](02_gitlab_depot_distant/03_00_etats.md)
- 4. [Pré-requis](02_gitlab_depot_distant/04_00_prerequis.md)
- 5. [GitLab](02_gitlab_depot_distant/05_00_gitlab.md)
  - 5.1 [GitLab et ForgeMIA](02_gitlab_depot_distant/05_01_gitlab_forgemia.md)
  - 5.2 [Configuration de l'accès à GitLab](02_gitlab_depot_distant/05_02_gitlab_acces.md)
  - 5.3 [Création de la clef SSH](02_gitlab_depot_distant/05_03_gitlab_ssh.md)
  - 5.4 [Création du projet GitLab](02_gitlab_depot_distant/05_04_gitlab_creation_projet.md)
- 6 Dépôt distant
  - 6. [Dépôt distant](02_gitlab_depot_distant/06_00_depot_distant.md)
  - 6.1 [Les commandes initiales](02_gitlab_depot_distant/06_01_commandes_initiales.md)
  - 6.2 [Vérification sur la forge](02_gitlab_depot_distant/06_02_verifications.md)
  - 6.3 [Gestion des étiquettes](02_gitlab_depot_distant/06_03_etiquettes.md)
- 7. [Fonctionnalités GitLab](02_gitlab_depot_distant/07_00_fonctionnalites_gitlab.md)
   - 7.1 [Membres d'un projet](02_gitlab_depot_distant/07_01_gestion_membres.md)
   - 7.2 [Issues](02_gitlab_depot_distant/07_02_issues.md)
- 8. [Bonnes pratiques d’interaction](02_gitlab_depot_distant/08_00_bonnes_pratiques.md)
- 9. [Prochaine session](02_gitlab_depot_distant/09_prochaine_session.md)

## Sessions suivantes

- Les branches
  - Intérêt
  - Pointeurs et HEAD
  - Divergence
  - Les différentes méthodes de fusion
  - Bonnes pratiques de fusion
- Remotes
  - Dépôt central et multi-dépôt distants
  - Configuration de l'accès à GitLab
  - Les commandes fetch et pull
  - La commande push
  - Bonnes pratiques d’interaction
- Commandes Git avancées
  - Reset et reflog
  - Rebasing, Squashing
  - Gestion du Stash
  - Blame
  - Recherche d’erreurs avec bisect
  - Cherry-pick
- Intégration Continue avec GitLab CI
  - Configuration GitLab CI/CD
  - Pipelines de CI/CD
  - Gestion des runners
- Déploiement continu et GitLab Runner
- Collaboration avec Git et GitLab CI
  - Merge requests
  - Rebase (+squash, +fast-forward)
  - Branches protégées
  - Wiki et documentation
  - Issues
- Workflows collaboratifs
  - Pourquoi utiliser un workflow
  - Les principaux workflows
  - Zoom sur Git Flow et mise en pratique
  - Github Flow et GitLab Flow
  - Mise en place d’un workflow de collaboration sur GitLab

## Matériel non utilisé :

- [Ressources](99/ressources.md)
- [Commandes principales](99/commandes_principales.md)
- [Configuration de Git en ligne de commande](99/configuration_cli.md)
- [fichier `.gitignore`](99/gitignore.md)
