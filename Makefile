SHELL := /bin/bash

BUILD_DIR := public
SESSIONS := 01_initiation 02_gitlab_depot_distant 03_branches
SESSION_DIRS := $(SESSIONS:%=$(BUILD_DIR)/%)
INDEXES := $(SESSIONS:%=$(BUILD_DIR)/%/index.html)
SLIDES := $(SESSIONS:%=$(BUILD_DIR)/%/slides.md)

$(INDEXES):: public/revealjs/index.html
	mkdir -p $(@D)
	cp $< $@
	sed -i -e 's:href=":href="../revealjs/:g' $@
	sed -i -e 's:src=":src="../revealjs/:g' $@

$(SLIDES):
	mkdir -p $(@D)
	cat $(shell basename $(@D))/??_*.md > $@
	cp -r $(shell basename $(@D))/images $(@D)

build: $(INDEXES) $(SLIDES)

clean:
	rm -fr $(SESSION_DIRS)

run: build
	(sleep 1 ; python3 -mwebbrowser http://0.0.0.0:8000/) &
	(cd public/ ; python3 -m http.server)

public/01_initiation/index.html::
	sed -i -e "s/SLIDESHOW_TITLE/Session 1 : initiation Git/g" $@

public/02_gitlab_depot_distant/index.html::
	sed -i -e "s/SLIDESHOW_TITLE/Session 2 : Gitlab et dépôt distant/g" $@

public/03_branches/index.html::
	sed -i -e "s/SLIDESHOW_TITLE/Session 3 : les branches/g" $@
