# Session 3 : les branches
#### Pratiquer

* Cas d'utilisation
* Principes
* Fusion (*merge*)
* Rebasage

Note: Créer une branche signifie diverger de la ligne principale de développement et continuer à travailler sans impacter cette ligne.
La manière dont Git gère les branches est incroyablement légère et permet de réaliser les opérations sur les branches de manière quasi instantanée et, généralement, de basculer entre les branches aussi rapidement. 
Git encourage des méthodes qui privilégient la création et la fusion fréquentes de branches, jusqu’à plusieurs fois par jour. Bien comprendre et maîtriser cette fonctionnalité vous permettra de faire de Git un outil puissant et unique et peut totalement changer votre manière de développer.
[source](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)
