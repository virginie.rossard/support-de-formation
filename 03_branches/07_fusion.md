---slide---

# Fusion, conflit, remisage
## *merge request*

---vertical---

## Graphe

```mermaid
gitGraph
   commit
   commit
   branch issue-1
   checkout issue-1
   commit id: "Correction de texte. Ticket #1"
   checkout main
   branch hotfix
   checkout hotfix
   commit id: "Correction urgente"
   checkout main
   merge hotfix
   checkout issue-1
   commit id: "Correction de code. Ticket #1"
   checkout main
   merge issue-1
```
Fusion de hotfix sur main puis de issue-1 sur main

---vertical---

## Pratique dans GitLab

GitLab permet de simplifier les étapes :

1. créer des tickets
2. créer une demande de fusion avec branche depuis chaque ticket
3. récupérer la branche en local
4. faire les modifications, valider, pousser
5. depuis GitLab, accepter la demande de fusion

---vertical---

## En ligne de commande

```sh
git switch -c issue-1
vim mon_texte.txt
git commit -m "Correction de texte. Ticket #1" mon_texte.txt
# Travail urgent à réaliser
git switch main
git switch -c hotfix
vim ma_config.xml
git commit -m "Correction urgente" ma_config.xml
# lancement de tests => OK, fusionner
git switch main
git merge hotfix
git branch -d hotfix
#
git switch issue-1
vim mon_texte.txt
git commit -m "Correction de texte terminée. Ticket #1" mon_texte.txt
git switch main
git merge issue-1
# l’historique de développement a divergé => commit de merge.
```

---vertical---

## Apparition et gestion des conflits

En cas de modifications différentes de la même partie du même fichier dans les deux branches, Git ne sera pas capable de réaliser proprement la fusion.

---vertical---

Git ajoute des marques de conflit standards dans les fichiers qui comportent des conflits, pour que vous puissiez les ouvrir et résoudre les conflits manuellement. Votre fichier avec conflit contient des sections qui ressemblent à ceci :

 ```
<<<<<<< HEAD:patch-1
<div id="footer">contact : email.support@github.com</div>
======
<div id="footer">
 please contact us at support@github.com
</div>
>>>>>>> patch-1:index.html
```

Note: HEAD:patch-1 affiche ce qui est dans le commit de patch-1.
L'utilisateur doit arbitrer entre les lignes séparées par `======`.

---vertical---

Il faut modifier les fichiers dans son éditeur ou avec `git mergetool`.

Utiliser `git status` pour afficher les conflits.

---vertical---

## Remisage

Dans le cas où :

* un travail est en cours, instable
* il n'est pas en état d'être validé (*commit*)
* une autre modification est à faire, dans une autre branche

`git stash` enregistre dans la pile des modifications non finies que vous pouvez ré-appliquer à n’importe quel moment.

---vertical---

```sh
git status
vim mon_texte.txt
git status
git stash
git status
git stash list
```

---vertical---

Pour retrouver ses modifications : `git stash apply`.

Git remodifie les fichiers non validés lorsque la remise a été créée.

Une remise peut être appliquée sur une branche différente de celle où elle a été créée.
Git vous indique les conflits de fusions si quoi que ce soit ne s’applique pas proprement.

`git stash drop <nom de la remise>` pour enlever une remise de la liste.

---vertical---

## Nettoyage

- supprimer les fichiers non-suivis : `git clean -f`.
- supprimer les fichiers non-suivis et dossiers vides non suivis : `git clean -f -d`
- supprimer aussi les fichiers correspondant à `.gitignore` : `git clean -f -x`

Note: [source](https://git-scm.com/book/fr/v2/ch00/s_git_stashing)

---vertical---

## Bonnes pratiques de fusion

Si vous attendez trop longtemps pour fusionner deux branches qui divergent rapidement, vous rencontrerez des problèmes.
