---slide---

# Pour aller plus loin

---vertical---
## Gestion de projet avec les branches

Deux grandes manières d’utiliser des branches existent

* [Le développement basé sur le tronc](https://www.atlassian.com/fr/continuous-delivery/continuous-integration/trunk-based-development)
* [Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow)

---vertical---
## Merge request (gitflow)

Un mode d'utilisation des branches basé sur un scénario "gitflow"

* Faire du merge request vers la branche dev
* Puis merge request de dev vers main
* A l'issue des merges, supprimer les branches (locales et distantes) sources

