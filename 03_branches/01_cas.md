---slide---

# Cas d'utilisation des branches

---vertical---

## Conditions d'usage des branches
* S’assurer que la branche `main` est toujours stable
* les branches créée le sont à partir de `main`
---vertical---

## Utilité des branches (1/2)
* Ne pas mélanger ses développements en cours
* Programmer en parallèle des collègues

---vertical---

## Utilité des branches (2/2)
* Faire des tests et expérimentations
* Laisser un travail en cours pour un autre plus urgent
  * Résolution de bug
  * Développement de fonctionnalité

---vertical---

## Exemple à ne pas suivre (1/2)

<!-- https://mermaid.live/edit#pako:eNqlkbFugzAQhl_FuhmhYGMw3iJFytSpnSovDj6IpRgjsKumiHcv0KhL6NT17vs_ne6foPYGQYLqWhvOg-6vqiOE1N45G4g1kig4No2tr7pFYiJpVwZJpuAZfMMxjET_4g-W7rEnvMT2mc322Q9y_JeVXOz4l3nvXkjA4eC0NctvpjWnIFzRoYI1ZrDR8RZW47ygOgb_eu9qkGGImEDsjQ54snoROpCNvo3LtNcdyAk-QbIqZUXJSkFZLgpGRQJ3kJTxtBD0wGmRUXbIyzmBL-8XQZZWnGeci7zKeVGKcpO9b7uHHI0Nfnj5KXPrdP4GMBOWSw -->
<!-- ![Une seule branche](images/01_une_seule_branche.png) -->

```mermaid
gitGraph
   commit id: "Dev fonction 1"
   commit id: "Ajout fonction 2"
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
```

Note: bien rappeller que cela peut poser problème

---vertical---

## Exemple à ne pas suivre (2/2)

Sur une seule branche :
* Du développement
* Du débogage
* Du test

Et le tout en même temps !

(C’est une mauvaise pratique)
