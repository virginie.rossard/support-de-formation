---slide---

# Principes des branches

---vertical---

## Définition d’une branche

Une branche est une **copie isolée** du code du projet

→ Le travail dans une branche n’affecte pas les autres

---vertical---

## Utilisation d’une branche

Scénario classique :

* Partir d’une branche stable (typiquement `main` ou `master`)
* Créer une nouvelle branche
* Résoudre **un** problème sur cette branche
  * Nouvelle fonctionnalité
  * Résolution de bugs
  * Tests de code/prototypage
* Une fois le problème résolu, fusionner la nouvelle branche dans la branche stable

---vertical---

## Exemple précédent avec des branches

---vertical---

## Exemple à suivre (1)

```mermaid
gitGraph
   commit id: "Dev fonction 1"
   branch Fonction-2
   commit id: "Ajout fonction 2"
   checkout main
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   checkout Fonction-2
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
```
Les fonctionnalités évoluent dans des branches séparées

---vertical---

## Fusion (merge) avec `main`

---vertical---

## Exemple à suivre (2)

```mermaid
gitGraph
   commit id: "Dev fonction 1"
   branch Fonction-2
   commit id: "Ajout fonction 2"
   checkout main
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   checkout main
   merge Bug-fonction-1
   checkout Fonction-2
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
   checkout main
   merge Fonction-2
```

---vertical---

L'exemple précédent fonctionne si:

* il n'y a pas de modifications sur main
* il n'y a pas un même fichier modifié sur chacune des branches


Sinon , il est nécessaire de faire un rebasage

---vertical---
Scénario tranquille (pas de nécessité d'un rebasage)
```mermaid
gitGraph
   commit
   commit
   branch Fonction-1
   commit id: "Dev fonction 1"
   checkout main
   merge Fonction-1
   branch Fonction-2
   commit id: "Ajout fonction 2"
   checkout main
   merge Fonction-2
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   checkout main
   merge Bug-fonction-1
   branch Evolution-Fonction-2
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
   checkout main
   merge Evolution-Fonction-2
   commit
```
---vertical---
Scénario bloquant (nécessite un rebasage)
```mermaid
gitGraph
   commit
   commit
   branch Fonction-1
   commit id: "Dev fonction 1"
   checkout main
   merge Fonction-1
   branch Fonction-2
   commit id: "Ajout fonction 2"
   commit id: "modif fonction commune (f2)"
   checkout main
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   commit id: "modif fonction commune (f1)"
   checkout main
   merge Bug-fonction-1
   merge Fonction-2
   commit
```
git branch Fonction-2 ; git rebase main

