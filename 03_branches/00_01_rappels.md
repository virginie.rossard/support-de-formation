---slide---

# Rappels de la session 1

Nous avons vu :

* intérêt de l'usage d'un outil comme git
* les différents états d'un dépôts local
* les commandes principales selon 3 modes
  * depuis l'interface de RStudio
  * en ligne de commande
  * avec l'éditeur VS Code

---vertical---

# Rappels de la session 2

Nous avons vu :

* GitLab : accès, configuration et fonctionnalités
* dépôt distant
* bonnes pratiques d'interaction
