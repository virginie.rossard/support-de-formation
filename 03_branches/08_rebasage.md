---slide---

# Rebasage

---vertical---

## Définition

Modifier l’origine d’une branche.

---vertical---

## Utilité

Rester à jour avec les modifications faites sur la branche d’origine

Permet de maintenir un historique de projet "linéaire"

Facilite les merge

---vertical---

## Commandes

Déplacer `ma_branche` sur la tête de la `branche_source`

```sh
# On s'assure d'avoir la dernière version de la branche source
git switch branche_source
git pull
# On bascule sur la branche à rebaser et on la rebase
git switch ma_branche
git rebase branche_source
```

---vertical---

## Illustration du problème (1/3)

```mermaid
gitGraph
   commit id: "Dev fonction 1"
   branch Fonction-2
   commit id: "Ajout fonction 2"
   checkout main
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   checkout main
   merge Bug-fonction-1
   checkout Fonction-2
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
   checkout main
   merge Fonction-2
```

Problème: les modifications apportées par le 1er merge ne sont pas intégrées à la branche `Fonction-2` avant de la fusionner dans `main`

---vertical---

## Solution du problème (2/3)

Rebaser la branche `Fonction-2` sur `main` après le merge de `Bug-fonction-1` :

```sh
# On s'imagine qu'on vient de faire le merge de Bug-fonction-1 sur main
# Par sécurité on s'assure que main est à jour avec le serveur
git switch main
git pull
# On passe sur Fonction-2
git switch Fonction-2
git rebase main
```

---vertical---

## Résultat (3/3)
Résultat :

```mermaid
gitGraph
   commit id: "Dev fonction 1"
   checkout main
   branch Bug-fonction-1
   commit id: "Debug fonction 1"
   commit id: "Test fonction 1"
   commit id: "Debug fonction 1 bis"
   checkout main
   merge Bug-fonction-1
   branch Fonction-2
   commit id: "Ajout fonction 2"
   commit id: "Dev fonction 2"
   commit id: "Dev++ fonction 2"
   checkout main
   merge Fonction-2
```

On constate qu'on a modifié l'historique pour le rendre linéaire

---vertical---

## Inconvénient

Le rebasage **réécrit l'historique des commits**

-> Problèmes si d'autres développeurs travaillent sur la même branche en même temps

---vertical---

## Recommandations

De préférence, rebaser uniquement des branches :
* Sur lequelles on travaille seul
* Pour lesquelles ont peut prévenir les autres développeurs de la réalisation d'un rebasage
