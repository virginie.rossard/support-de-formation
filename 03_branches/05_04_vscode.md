---slide---

# Gestion des branches locales
## avec VS Code
(extension source control)

---vertical---
## Interface


![Zones d'interaction](images/05_04_vscode_IHM.png)

---vertical---

## Création

![Création](images/05-04_vscode_nvlle_branche.png)

* crée une branche nommée `patch-1`
* bascule dessus

Note: `git branch patch-1 ; git checkout patch -1` ou `git checkout -b patch-1` avant git-2.23

---vertical---

```mermaid
gitGraph
   commit
   commit
   branch patch-1
   checkout patch-1
   commit
```

Note: Cela déplace HEAD pour le faire pointer vers la branche testing.
La branche `patch-1` a avancé tandis que la branche `main` pointe toujours sur le commit sur lequel vous étiez lorsque vous avez lancé la commande `git checkout` pour changer de branche.
Les branches ne coûtent quasiment rien à créer et à détruire.

---vertical---

### À partir d'une étiquette existante (1/2)

* Créer une étiquette `v2.9.1`
![Création etiquette](images/05-04_vscode_create_tag1.png)

---vertical---

### À partir d'une étiquette existante (2/2)

* clic sur la branche en cours
* Menu "Create branch from ..."
* Sélection de l'étiquette `v2.9.1`
* Nommage de la nouvelle branche `patch-1`
* Bascule dessus automatiquement

```mermaid
gitGraph
   commit
   commit
   commit tag: "v2.9.1"
   branch patch-1
   checkout patch-1
   commit
   checkout main
   commit
   commit
```

---vertical---

## Lister les branches

![lister branche](images/05-04_vscode_liste_branches.png)

---vertical---

## Supprimer une branche locale

![suppr branche](images/05-04_vscode_suppr_branche.png)

* sélectionner la branche à supprimer
* la branche en cours ne peut être supprimée


---vertical---

## Travailler avec plusieurs branches simultanément

Basculer d'une branche à une autre

![changement de branche](images/05_04_vscode_IHM.png)



Note: Changer de branche modifie les fichiers dans votre répertoire de travail : le dossier se retrouve dans le même état que lors du dernier commit sur cette branche.
Il est important de noter que lorsque vous changez de branche avec Git, les fichiers de votre répertoire de travail sont modifiés. Si vous basculez vers une branche plus ancienne, votre répertoire de travail sera remis dans l’état dans lequel il était lors du dernier commit sur cette branche. Si git n’est pas en mesure d’effectuer cette action proprement, il ne vous laissera pas changer de branche.
[source](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)
