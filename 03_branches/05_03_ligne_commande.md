---slide---

# Gestion des branches locales
## en ligne de commande

---vertical---

## Création

```sh
# depuis git-2.23 (août 2019)
git branch patch-1
git switch patch-1
# ou en raccourci
git switch -c patch-1
# Ou si switch n'est pas disponible
git checkout patch-1
```

* créé une branche nommée `patch-1`
* et bascule dessus

Note: `git branch patch-1 ; git checkout patch -1` ou `git checkout -b patch-1` avant git-2.23

---vertical---

```mermaid
gitGraph
   commit
   commit
   branch patch-1
   checkout patch-1
   commit
```

Note: Cela déplace HEAD pour le faire pointer vers la branche testing.
La branche `patch-1` a avancé tandis que la branche `main` pointe toujours sur le commit sur lequel vous étiez lorsque vous avez lancé la commande `git checkout` pour changer de branche.
Les branches ne coûtent quasiment rien à créer et à détruire.

---vertical---

### À partir d'une étiquette existante

```sh
git checkout -b patch-1 v2.9.1
```

* crée une branche `patch-1` à partir de l'étiquette `v2.9.1`
* bascule dessus.

```mermaid
gitGraph
   commit
   commit
   commit tag: "v2.9.1"
   branch patch-1
   checkout patch-1
   commit
   checkout main
   commit
   commit
```

---vertical---

## Lister les branches

```sh
git branch
```

```
  main
* patch-1
```

NB : l'historique affiche le nom des branches

```sh
git log --oneline --decorate
```

```
e82b921 (HEAD -> patch-1, tag: v2.9.1, main) init
```

---vertical---

## Supprimer une branche locale

```sh
# supprimer une branche totalement fusionnée (-d ou --delete)
git branch --delete patch-1
# supprimer une branche (même non fusionnée)
git branch -D patch-1
```

---vertical---

## Travailler avec plusieurs branches simultanément

Basculer d'une branche à une autre

```sh
git checkout main
git checkout patch-1
git checkout patch-2
# ou depuis git-2.23
git switch patch-1
git switch - # bascule sur la précédente branche (patch-1 ici)
# connaître la branche en cours
git branch
# montrer l’historique de la branche
git log patch-1
# montrer l’historique de toutes les branches
git log --all
```

Note: Changer de branche modifie les fichiers dans votre répertoire de travail : le dossier se retrouve dans le même état que lors du dernier commit sur cette branche.
Il est important de noter que lorsque vous changez de branche avec Git, les fichiers de votre répertoire de travail sont modifiés. Si vous basculez vers une branche plus ancienne, votre répertoire de travail sera remis dans l’état dans lequel il était lors du dernier commit sur cette branche. Si git n’est pas en mesure d’effectuer cette action proprement, il ne vous laissera pas changer de branche.
[source](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)
