---slide---

# Gestion des branches locales
## RStudio

---vertical---

## Créer une nouvelle branche 

![Créer une branche sur RStudio](images/05_02_creer_branche_Rstudio2.png)

---vertical---

## Pour importer une branche créée depuis gitlab 

`git pull` ou en cliquant sur le bouton `pull` de RStudio.

![git pull branhche gitlab](images/05_02_git_pull_branch.png)

---vertical---

## À partir d'une étiquette existante

En ligne de commande (dans le terminal Rstudio) :

```sh
git checkout -b nom_de_ma_branche v2.9.1
```

* crée une branche `nom_de_ma_branche` à partir de l'étiquette `v2.9.1`
* bascule dessus

---vertical---

## Lister les branches (1/2)

![Visualiser les branches](images/05_02_visu_branche_rstudio.png)

---vertical---

## Lister les branches (2/2)

Via la fenêtre de commit

![Visualiser les branches](images/05_02_visu_branche_rstudio2.png)

---vertical---

## Supprimer une branche 

(dans le terminal de Rstudio)
```sh
# pour visualiser les branches locales
git branch
# pour supprimer les branches locales
git branch -d nom_de_ma_branche
# pour visualiser les branches restantes
git branch
```
