---slide---

# Bonnes pratiques pour les branches

---vertical---

* Ne pas faire de commit sur la branche `main` !
* ```git status``` au début d'une séquence de travail pour vérifier sur quelle branche on est
* ```git pull``` pour récupérer d'éventuels commit 
* Créer une branche pour chaque bug ou évolution
* Toujours créer une branche à partir du tronc (main ou master)
* Avant de changer de branche, commit ou stash (bien nommés)
* Ne pas trainer pour fusionner

