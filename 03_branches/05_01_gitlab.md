---slide---

# Gestion des branches Gitlab

---vertical---

Dans l'interface de gitlab, on peut:

* visualiser la liste des branches (1)
* visualiser le dépôt pour une branche (2)
* supprimer une branche (3)

Il est également possible de déclencher des "merge request"

---vertical---

![branches dans gitlab](images/05-0_gitlab_branches.png)

---vertical---

Cliquez sur le nom d'une branche pour voir le dépôt dans l'état correspondant

![une branche](images/05-01_gitlab_une_branche.png)

---vertical---

## Création d'une branche (1/3)

Via une issue

![creation par issue](images/05-gitlab_create_branch_from_issue.png)

---vertical---

## Création d'une branche (2/3)

Via le menu branche
![creation par menu branche](images/05-gitlab_creation_par_menu_branche.png)

---vertical---

## Création d'une branche (3/3)

Avec ces méthodes de création d'un branche depuis gitlab, il faut récupérer la nouvelle branche en local

```sh
# Lister toutes les branches disponibles (locales et distantes)
git branch -a 
# Récupérer le contenu de la branche
git pull origin la_branche_distante
# se mettre dessus
git checkout la_branche_distante
```
