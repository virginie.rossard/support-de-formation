---slide---

# Gestion des branches distantes
## en ligne de commande

---vertical---

```sh
# Récupérer une branche distante :
git fetch
git switch patch-1
# Récupérer les modifications de la branche distante :
git pull
# Envoyer les modifications sur la branche distante :
git push origin patch-1
# Supprimer une branche distante
git push -d origin remote-patch-1
```

avec `origin` l'alias du dépôt distant (par défaut).

NB : possibilité de renommer la branche.

Note: `git pull` = `fetch + rebase ou merge`
---vertical---

## Dans quelle branche suis-je ?

```sh
$ git status
```

```
On branch local-patch-1
Your branch is up to date with 'origin/remote-patch-1'.
```

## Quelles sont les branches ?

```sh
$ git branch
# lister aussi les branches distantes
$ git branch -a
```
