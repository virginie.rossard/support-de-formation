---slide---

# Gestion des branches distantes
## avec VS Code


---vertical---
# Récupérer une branche distante :
![nvelle branche from ](images/06-03_vscode_nvlle_branche_from.png)



---vertical---
## Récupérer les modifications de la branche distante :
synchroniser les modifications locales avec celles distantes

![synchro branche](images/06-03_vscode_synchro_branches.png)
---vertical---
## Envoyer les modifications sur la branche distante :
L'icone identifié apparait tant que la branche locale n'existe pas sur le serveur distant

![publier branche](images/06-03_vscode_publish_branch.png)


---vertical---
# Supprimer une branche distante

Impossible de faire cela avec l'interface VS code.
Il est nécessaire de passer par la ligne de commande, dans le terminal.
```sh
git push -d origin remote-patch-1
```



---vertical---

## Dans quelle branche suis-je ?

![ou suis-je ?](images/06-03-vscode-ousuisje.png)


---vertical---

## Quelles sont les branches ?


![lister branche](images/05-04_vscode_liste_branches.png)

